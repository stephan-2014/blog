# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = 'e3fa779f97cb430a6243167356983089db5e39afa3860d81828f70fd96b6b5c9798edc5807c63a443fc9516407c58f6aa232cb05737d891536a43d77ea960ca5'
